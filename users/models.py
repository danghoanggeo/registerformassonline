from django.db import models
import datetime
from django.contrib.auth.models import User
from django.utils import timezone
from PIL import Image


class Profile(models.Model):
    health_choice = (
        ('GOOD','Good'),
        ('UNWELL','Unwell'),
        ('SICK','Sick')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_first_name = models.CharField(u'Your first name',default='',max_length=30)
    profile_last_name = models.CharField(u'Your last name',default='',max_length=30)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    address = models.CharField(default='',max_length=300)
    age = models.SmallIntegerField(default=0)
    phone_number = models.CharField(default='',max_length=12)
    health_status = models.CharField(max_length=15,choices=health_choice,default="GOOD")
    last_check_tempture = models.SmallIntegerField(u'Your last tempture checked',default=36)
    last_check_tempture_time = models.DateField(u'The time your checked tempture',default=datetime.date.today)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self,*args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)