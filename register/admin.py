from django.contrib import admin

# Register your models here.
from .models import Mass, Register, Event, Booking, Seat

# Register your models here.
admin.site.register(Mass)
admin.site.register(Register)
admin.site.register(Event)
admin.site.register(Seat)
admin.site.register(Booking)