from django import forms
from .models import Event, Booking

class BookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = ('')