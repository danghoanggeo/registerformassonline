from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.conf import settings
import sys
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import(
    ListView,
    DeleteView,
    CreateView,
    UpdateView,
    DeleteView
)
from users.forms import ProfileUpdateTemptureForm
from .models import Register, Mass
from .models import Event, Booking, Seat
from users.models import Profile
from django.contrib.auth.models import User


# Create your views here.

def home(request):
    context = {
        'events': Event.objects.all()
    }
    return render(request,'register/home.html', context)

class RegisterCreateView(LoginRequiredMixin, CreateView):
    model = Register
    fields = ['mass','seat_number','qa_code_text','status']

    def form_valid(self,form):
        form.instance.author = self.request.user
        username = form.instance.author.username
        messages.success(self.request, f'Mass Registered for {username}!')
        return super().form_valid(form)

class MassListView(ListView):
    queryset = Event.objects.filter(event_date__gte = timezone.now())   #__lte less than or equal; __gte greater than or equal
    template_name = 'register/home.html' #<app>/<model>_<viewtype>.html
    context_object_name = 'events'
    ordering = ['event_date','event_time']

def about(request):
    return render(request,'register/about.html')

@login_required
def booking(request,pk):
    if request.POST:
        user_temp = request.POST.get('user_temp')
        user_temp_date = request.POST.get('user_temp_check_date')
        print(user_temp_date)
        try:
            user = request.user
            event_id = request.POST.get('event_id')
            print("eventid: "+event_id)
            event = Event.objects.get(pk=event_id)
            print(event.event_slots_registered)
            #Check if user have registered for this event
            if Booking.objects.filter(booking_user=user,booking_event=event):
                messages.warning(request, f'Your account has been registered for this Mass!')
                return render(request,'register/registerfail.html')

            #Save user profile update tempturate self checked
            print(user.username)
            users_profile = user.profile
            users_profile.last_check_tempture = int(user_temp)
            print(users_profile.last_check_tempture)
            users_profile.last_check_tempture_time = user_temp_date
            print(users_profile.last_check_tempture_time)
            users_profile.save()
            print("User profile saved")

            # Booking save
            seat = Seat.objects.get(pk=1)
            print(seat.seat_no)
            booking = Booking(booking_user=user,booking_event=event,booking_seat=seat)
            booking.save()
            print(booking.booking_confirm_code)
            #print(ALLOWED_HOSTS)
            booking.booking_code = settings.HOST_NAME+"confirmmassattend/?prshaid="+str(user.pk)+"&bootouroid="+str(booking.pk)+"&ebenjobid="+str(event_id)+"&adncode="+booking.booking_confirm_code
            print(booking.booking_code)
            booking.save()
            #minus 1 slot
            event.event_slots_registered += 1
            event.save()
            print("Booking saved")
        except:
            print("Internal exception")
            print("Unexpected error: ", sys.exc_info()[0])
            messages.warning(request, f'Something wrong has happened, Please contact the admin')
            return render(request,'register/registerfail.html')
        messages.success(request, f'Your registration for this Mass is successfully!')
        return redirect('register-home')
    else:
        event = Event.objects.get(pk=pk)
        #p_form = ProfileUpdateTemptureForm(instance=request.user.profile)
        #Check whether user is valid to resevate
        context = {
            'event':event
        }
        return render(request,'register/register_form.html',context)

@login_required
def bookingdone(self,request):
    if request.POST:
        user_temp = request.POST.get('user_temp')
        user_temp_date = request.POST.get('user_temp_check_date')
        try:
            #Save user profile update tempturate self checked
            user = self.request.user
            users_profile = Profile.objects.get(user=user)
            #users_profile.last_check_tempture = int(user_temp)
            users_profile.last_check_tempture_time = user_temp_date
            users_profile.save()

            # Booking save
            event_id = request.POST.get('event_id')
            event = Event.objects.get(pk=event_id)
            seat = 0
            booking = Booking(booking_user=user,booking_event=event,booking_seat=seat)
            booking.save()
        except:
            print("abc")
        else:
            return redirect('register/registerfail.html')
        return render(request, 'register/register_confirmation.html')

@login_required
def confirmmassattend(request):
    if request.user.groups.filter(name="Admin").exists():
        user_id = request.GET.get('prshaid',0)
        user = User.objects.get(pk=user_id)
        booking_id = request.GET.get('bootouroid',0)
        event_id = request.GET.get('ebenjobid',0)
        code = request.GET.get('adncode','none')
        booking = Booking.objects.get(pk=booking_id,booking_confirm_code=code)
        if booking.booking_status == "A" or booking.booking_status == "W" :
            booking.booking_status = "P"
            booking.save()
            messages.success(request, f'Welcome '+user.username +' !')
            return redirect('register-home')
        else:
            messages.warning(request, f'This registration is not correct!')
            return redirect('register-home')
    else:
        messages.warning(request, f'You are not allowed to approve it!')
        return redirect('register-home')


@login_required
def myregistered(request):

    user = request.user
    booking = Booking.objects.filter(booking_user=user)
    if booking:
        context = {
            'mybooking':booking
        }
        return render(request,'register/myregistered.html',context)
    else:
        messages.warning(request, f'You did not make any registration!')
        return redirect('register-home')

