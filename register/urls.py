from django.urls import path
from . import views
from .views import (
    RegisterCreateView,
    MassListView
)

urlpatterns = [
    path('', MassListView.as_view(), name='register-home'),
    path('about/',views.about, name='register-about'),
    path('myregistration/',views.myregistered, name = 'my-register'),
    path('eventregister/<int:pk>/',views.booking,name='register-create'),
    path('eventregisterdone/',views.bookingdone,name='book-confirm'),
    path('confirmmassattend/',views.confirmmassattend,name='confirmmassattend'),
    path('myregistration/<int:pk>/cancel',RegisterCreateView.as_view(),name='register-cancel')
]