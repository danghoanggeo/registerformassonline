from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.contrib.auth.models import User


class Mass(models.Model):
    language_choice = (
        ('JAPANESES','日本語'),
        ('ENGLISH','English'),
        ('SPANISH','Spanish'),
        ('VIETNAMESE','Tiếng Việt')
    )
    mass_date = models.DateField()
    mass_time = models.TimeField()
    mass_title = models.CharField(max_length=100)
    mass_language = models.CharField(max_length=15,choices=language_choice)
    mass_date_ordinary = models.CharField(max_length=100)
    main_celebrant = models.CharField(max_length=40)
    mass_slots = models.SmallIntegerField(default=100)
    mass_slots_registered = models.SmallIntegerField(default=0)
    mass_online_url = models.URLField(blank=True)
    mass_image = models.ImageField(null=True,blank=True,upload_to='mass_images')

class Register(models.Model):
    date_registered = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mass = models.ForeignKey(Mass, on_delete=models.CASCADE)
    seat_number = models.CharField(max_length=100)
    qa_code_text = models.CharField(max_length=200)
    status = models.CharField(max_length=2)

class Event(models.Model):
    language_choice = (
        ('JAPANESES','日本語'),
        ('ENGLISH','English'),
        ('SPANISH','Spanish'),
        ('VIETNAMESE','Tiếng Việt')
    )
    event_date = models.DateTimeField()
    event_time = models.TimeField()
    event_time_length = models.SmallIntegerField(blank=True,default=1)
    event_title = models.CharField(max_length=100)
    event_content = models.TextField(blank=True)
    event_language = models.CharField(max_length=15,choices=language_choice)
    event_date_ordinary = models.CharField(max_length=100)
    event_holder = models.CharField(max_length=40)
    event_slots = models.SmallIntegerField(default=100)
    event_slots_registered = models.SmallIntegerField(default=0)
    event_online_url = models.URLField(blank=True)
    event_image = models.ImageField(null=True,blank=True,upload_to='mass_images')
    event_active = models.BooleanField(default=True)
    event_created_date = models.DateTimeField(default=timezone.now)

class Seat(models.Model):
    seat_choice = (
        ('','Select'),
        ('CHILDREN','For Children'),
        ('OLDER','For Older People'),
        ('NOTWELL','For UnHeathy Peopel'),
        ('YOUNG','For Young People')
    )
    event_choice = (
        ('MASS','Mass'),
        ('EVENT','Event')
    )
    seat_no = models.CharField(max_length=4,null=True,blank=False)
    seat_type = models.CharField(max_length=30,choices=seat_choice, blank=False)
    seat_event = models.CharField(max_length=8,choices=event_choice, blank=False)

    class Meta:
        unique_together = ('seat_no','seat_event')

    def __str__(self):
        return self.seat_no + str(self.seat_event)

class Booking(models.Model):

    status_choice = (
        ('W','Waiting'),
        ('A','Approved'),
        ('D','Denied'),
        ('P','Presented'),
        ('AB','Absented')
    )
    random_code = get_random_string(length=18, allowed_chars='ABCDEFGHabcdefgh12345YUyu')
    booking_date = models.DateTimeField(default=timezone.now)
    booking_user = models.ForeignKey(User, on_delete=models.CASCADE)
    booking_event = models.ForeignKey(Event, on_delete=models.CASCADE)
    booking_seat = models.ForeignKey(Seat, on_delete=models.CASCADE)
    booking_confirm_code = models.CharField(max_length=18,default=random_code)
    booking_code = models.CharField(max_length=300,default='')
    booking_status = models.CharField(max_length=12,choices=status_choice,default='W')
    booking_attend_time = models.DateTimeField(default=timezone.now)
    
    class Meta:
        unique_together = ('booking_seat','booking_event')
    
    def __str__(self):
        return str(self.booking_user)+'-'+str(self.booking_event)+'-'+self.booking_confirm_code
    
    

def event_index(request):
    event_list = Event.object.filter(event_date>=timezone.now, event_active=True)

# Create your models here.
